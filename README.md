# 一、注解版导出
## （1）主要功能：
1、支持单sheet表格导出功能    
2、支持多sheet导出功能（每个sheet可以是不同对象）    
3、支持注解完成表头内容的设置，宽度设置，高度设置（不设自适应高度）    
4、支持注解完成日期类型格式化(不设置检查出是日期类型自动格式化为YYYY-MM-dd)  
5、支持注解完成空时显示默认值功能     
6、支持注解完成sheet名称设置，不设置默认为表格名称，出现重复名称，设置为名称+序号。  
7、支持动态导出，导出选择列或者排除所选列  
8、支持导出文件到指定的路径下
## （2）未来规划：
1、支持复杂表头类型表格导出  
2、支持筛选条件动态导出功能（导出数据的同时，想带时间、案由等筛选条件需求）  

## （3）浏览器导出表格
### 1、引入坐标
```
		<dependency>
			<groupId>com.thunisoft</groupId>
			<artifactId>excel-tool</artifactId>
			<version>0.0.9-SNAPSHOT</version>
		</dependency>
```

### 2、在要导出对象类加@Excel注解  
#### 属性上的@Excel注解（必选项）  
name = "日志时间" ： 表头名称（必填）    
column = 0 ： 列号，从0开始（必填，根据表格展示顺序设置）  
width = 150 ：列宽 （可选默认为30）  
dateFormat = "YYYY-MM"  ：日期类型格式化（可选，不设置检查出是日期类型默认格式化为 YYYY-MM-dd ）  
defaultValue ="查询" ： 空时默认值（可选）  
high = 100 ：表头高度 （可选，不设置自适应高度）

#### 类上的@Excel注解 （可选项）   
类上的注解目前只有名称，为sheet名称，不指定默认为表格名称  

### 例子：
```
@Data
@Excel(name="日志")
public class OperationLog {

    @ApiModelProperty("Id")
    private String id;

    @Excel(name = "日志时间",column = 0,width = 150，dateFormat = "YYYY-MM")
    @ApiModelProperty("日志时间")
    private Date logTime;
    @Excel(name = "账号",column = 1)

    @ApiModelProperty("账号")
    private String account;
    @Excel(name = "所在单位",column = 2)

    @ApiModelProperty("所在单位")
    private String corp;


    @Excel(name = "日志类型",column = 3)
    @ApiModelProperty("日志类型")
    private String logType;

    @Excel(name = "操作",column = 4 ,defaultValue ="查询")
    @ApiModelProperty("操作")
    private String operation;
}
```

### 3、调导出方法  

1. 多sheet导出方法   
exportXls.export(HttpServletResponse response, List<ExcelData<T>> excelDataList, String name);  
2. 单sheet导出方法   
exportXls.export(HHttpServletResponse response, ExcelData<T> excelData, String name);  
例子（多sheet，单sheet同理）  
```
//注入
private final AnnotationExcelExport<CaseDetail> annotationExcelExport;

    public void exportTest(HttpServletResponse response){
        List<IndicatorDetail> indicatorDetails = getIndicatorDetails(indicatorDetailList);  //数据要导出的数据
        ExcelData<IndicatorDetail> excelData = new ExcelData<>();
        excelData.setClassType(IndicatorDetail.class);
        excelData.setData(indicatorDetails);
        caseRemoveSuspectName(param.getUnit(), excelData);   //动态导出功能。没有此功能可以跳过此步
        annotationExcelExport.export(response, excelData, param.getName());
    }
        

    /**
     * 案件判断是否是案件，是案件导出对象中的嫌疑人姓名移除
     * @param unit 单位（件/人）
     * @param excelData 表格数据
     */
    private void caseRemoveSuspectName(String unit, ExcelData<IndicatorDetail> excelData) {
        if(StringUtils.equals(unit, CalculationConsts.PIECE_UNIT)){
            List<String> columnName = new ArrayList<>();
            columnName.add("suspectName");
            ColumnInfo columnInfo = new ColumnInfo();
            columnInfo.setColumnName(columnName);
            excelData.setColumnInfo(columnInfo);
        }
    }
```
### 4、动态导出功能（可选）
动态导出有2中方式，一种是排除不想要的字段，另一种方式是包含想要的字段。默认是排除如上面例子所示
，排除suspectName字段位例子

(1) 排除字段
```
ExcelData<IndicatorDetail> excelData = new ExcelData<>()
 List<String> columnName = new ArrayList<>();
            columnName.add("suspectName");
            ColumnInfo columnInfo = new ColumnInfo();
            columnInfo.setColumnName(columnName);
            excelData.setColumnInfo(columnInfo);
```
（2）要包含的字段
```
ExcelData<IndicatorDetail> excelData = new ExcelData<>()
 List<String> columnName = new ArrayList<>();
            columnName.add("suspectName");
            ColumnInfo columnInfo = new ColumnInfo();
            columnInfo.setColumnName(columnName);
            columnInfo.setIsExclude(false);  //是否排除设置成包含。默认是排除
            excelData.setColumnInfo(columnInfo);
```
## （4）导出表格文件到指定路径
```
annotationExcelExport.saveFile(excelData, filePath,fileName);
```
- excelData： ExcelData<IndicatorDetail> 表格数据，跟上方构建方式相同
- filePath ：要导出的路径，没有则新建
- fileName ：导出文件名



# 二、模板版本导出
## （1）主要功能：
1、对于复杂表头，会花费大量的时间去写合并信息及表头信息，为了简化这部分时间设计一种模板方式，即不需要关注复杂表头，只需要灌入数据，就可以实现复杂表头的表格的导出功能。  
2、同时也添加了动态数据设置，应用场景主要为导出数据的同时要带筛选时间，案由等。  
3、新增模板背景色样式。
## （2）未来版本规划：
1、添加动态导出功能  
2、支持通过类方式的导出功能  
3、添加单sheet导出模式，简化设置流程  

## （3）使用步骤
### 1、引入坐标
```
        <dependency>
			<groupId>com.thunisoft</groupId>
			<artifactId>excel-tool</artifactId>
			<version>0.0.9-SNAPSHOT</version>
		</dependency>
```
### 2、添加表格模板（主要配置，见下面详情。模板例子见git里面的例子文件）  
#### （1）根据项目要求设置表头等信息可以参考例子表格。  
    1、必要条件 >>>:data_start（数据开始位子） >>>:data_end（程序结束位子）      
    2、可选条件 已 >>>: 开头自定义数据信息例子（>>>:time），作用是替换特殊数据（时间，案由等筛选条件，需要跟数据一起导出需求）    
#### （2）添加表格模板位子（可选）
 excel:
  tool:
    directory: xxxx
### 3、调导出方法
export(HttpServletResponse response, List<ExcelTemplateData> data, String name)
#### 例子：
```
      /**
     * 表格导出第一个大表格
     * @param response respose参数
     * @param exportParam 要导的数据信息
     * @param fileName 文件名称
     */
    public void tableExport(HttpServletResponse response, ExportParam exportParam, String fileName) {

        List<List<String>> data = exportParam.getData();  //将数据变成行数据（List<String>），根据自身需求构建数据对象
        ExcelTemplateData excelTemplateData = new ExcelTemplateData();
        excelTemplateData.setStandardData(data);
        Map<String,String> specialData = new HashMap<>(1);
        specialData.put(">>>:name",exportParam.getName()); //构建特殊数据替换，这里要替换的是表里里面的名称
        excelTemplateData.setSpecialData(specialData);
        List<ExcelTemplateData> data = new ArrayList<>();
        data.add(excelTemplateData);
        exportXls.export(response,data,fileName);
    }
```

# 三、读取导入表格数据
### 1、引入坐标
```
<dependency>
			<groupId>com.thunisoft</groupId>
			<artifactId>excel-tool</artifactId>
			<version>0.0.9-SNAPSHOT</version>
		</dependency>
```
### 2、注入ReadXlsData

```
@RequiredArgsConstructor(onConstructor = @__(@Autowired))

private final ReadXlsData readXlsData;
```

### 3、传入流调用获取数据方法（多sheet数据，单sheet获取一个即可）
```
List<List<XlsDataInfo>> xlsData = readXlsData.getXlsData(inputStream);
```

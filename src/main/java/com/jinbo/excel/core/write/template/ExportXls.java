package com.jinbo.excel.core.write.template;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;

import com.jinbo.excel.core.read.ReadXlsTemplate;
import com.jinbo.excel.core.write.CommonExportXls;
import com.jinbo.excel.model.CustomizeCellRangeAddress;
import com.jinbo.excel.model.ExcelTemplateData;
import com.jinbo.excel.model.SheetInformation;
import com.jinbo.excel.model.XlsDataInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * <p> Description: 导出表格类</p >
 *
 * <p> CreationTime: 2020/10/31 18:33
 * <br>Copyright: 2020 < a href=" ">Thunisoft</ a>
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */
@Slf4j
public class ExportXls {
    private static final String MARK = ">>>:";
    private static final String DATA_END = ">>>:data_end";
    private static final String DATA_START = ">>>:data_start";
    private ReadXlsTemplate readXlsTemplate;
    public ExportXls(ReadXlsTemplate readXlsTemplate) {
        this.readXlsTemplate = readXlsTemplate;
    }

    /**
     *  导出表格
     * @param response response
     * @param data 数据
     * @param name 名称
     * @return 导出过程信息
     */
    public String export(HttpServletResponse response, List<ExcelTemplateData> data, String name)  {
        HSSFWorkbook wb = getWorkbook(data, name);
        String excelName = "";
        try {
            excelName = URLEncoder.encode(name, "utf-8");
        } catch (UnsupportedEncodingException e) {
            log.error("文件名编码失败{}",name);
            e.printStackTrace();
        }
        CommonExportXls.fileTransmission(response, wb, excelName+".xls");
        return "导出成功";
    }

    /**
     * 导出表格为 HSSFWorkbook 格式
     * @param data 数据
     * @param name 名称
     * @return 结果
     */
    public HSSFWorkbook getWorkbook(List<ExcelTemplateData> data, String name) {
        Map<String, List<SheetInformation>> xlsInformation = readXlsTemplate.getXlsInformation();
        HSSFWorkbook wb = new HSSFWorkbook();
        List<SheetInformation> sheetInformationList = xlsInformation.get(name+".xls");
        if(sheetInformationList==null || sheetInformationList.isEmpty()){
            log.error("传入的表格没有找到请检查表名及配置模板路径是否正确");
        }
        for(int i=0;i< data.size();i++){
            SheetInformation sheetInformation = sheetInformationList.get(i);
            HSSFSheet sheet = wb.createSheet(sheetInformation.getSheetName());
            List<XlsDataInfo> xlsDataInfoList = sheetInformation.getXlsDataInfos();
            List<CustomizeCellRangeAddress> customizeCellRangeAddresses = sheetInformation.getCustomizeCellRangeAddresses();

            Map<String,String> specialData = new HashMap<>();
            List<List<String>> standardData = new ArrayList<>();
            if(i<data.size()){
                ExcelTemplateData excelData = data.get(i);
                specialData = excelData.getSpecialData();
                standardData = excelData.getStandardData();
            }
            Map<String , XlsDataInfo> map =
                    xlsDataInfoList.stream().filter(xlsDataInfo ->
                            xlsDataInfo.getData().contains(MARK)).
                            collect(Collectors.toMap(XlsDataInfo::getData, Function.identity()));
            XlsDataInfo endDataAddressInfo = map.get(DATA_END);
            XlsDataInfo startDataAddressInfo = map.get(DATA_START);
            if(endDataAddressInfo==null || startDataAddressInfo==null ){
                log.error("模板错误，请检测模板是否有数据起始结束标识>>>:data_end>>>:data_start");
            }
            //设置内容样式
            Optional<XlsDataInfo> maxFixedNameAddressInformation =
                    xlsDataInfoList.stream()
                            .max(Comparator.comparing(XlsDataInfo::getRowIndex));

            XlsDataInfo xlsDataInfo = maxFixedNameAddressInformation
                    .orElse(XlsDataInfo.builder().build());
            int fixedNameMaxRow = xlsDataInfo.getRowIndex()+1;
            int dataMaxRow = data.get(i).getStandardData().size() + endDataAddressInfo.getRowIndex();
            int maxRow = Math.max(fixedNameMaxRow, dataMaxRow);
            CommonExportXls.setFormContentStyle(wb, sheet, maxRow,
                    endDataAddressInfo.getCellNum(),sheetInformation);
            //合并单元格
            mergeForm(sheet,customizeCellRangeAddresses);
            //写入固定信息
            fillInFixedInformation(sheet, xlsDataInfoList,specialData);
            //填录数据
            fillInFarmData(sheet, standardData,endDataAddressInfo.getRowIndex(),startDataAddressInfo.getCellNum());

        }
        return wb;
    }

    /**
     *  填入统计表二数据
     * @param sheet 表格类
     * @param list 填入的数据
     */
    private void fillInFarmData(HSSFSheet sheet, List<List<String>> list,int rowOffset,int cellOffset) {
        for(int i = 0; i < list.size();i++){
            int rowNum = i + rowOffset;
            for(int j=0;j< list.get(i).size();j++){
                int cell = j + cellOffset;
                sheet.getRow(rowNum).getCell(cell).setCellValue(list.get(i).get(j));
            }
        }
    }
    /**
     *  合并单元格
     * @param sheet 表格HSSFSheet对象
     * @param customizeCellRangeAddresses 表格合并信息
     */
    private void mergeForm(HSSFSheet sheet, List<CustomizeCellRangeAddress> customizeCellRangeAddresses) {
        customizeCellRangeAddresses.forEach(info ->
                sheet.addMergedRegion(new CellRangeAddress(info.getFirstRow(),
                        info.getLastRow(),
                        info.getFirstColumn(), info.getLastColumn())));
    }

    /**
     * 写入固定信息
     * @param sheet 表格HSSFSheet对象
     * @param xlsDataInfoList 固定表头信息
     */
    private void fillInFixedInformation(HSSFSheet sheet,
                                        List<XlsDataInfo> xlsDataInfoList,
                                        Map<String,String> specialData ) {
        for(XlsDataInfo xlsDataInfo : xlsDataInfoList){
            String fixedName = xlsDataInfo.getData();
            String fixedReplaceName="";
            if(fixedName.contains(MARK)&&specialData!=null){
                fixedReplaceName = specialData.get(fixedName);
            }
            addFixedData(sheet, xlsDataInfo,fixedReplaceName);
        }
    }

    /**
     * 填入固定信息
     * @param sheet 表格信息
     * @param xlsDataInfo 固定信息
     * @param fixedReplaceName 替换信息
     */
    private void addFixedData(HSSFSheet sheet
            , XlsDataInfo xlsDataInfo, String fixedReplaceName) {
        int rowIndex = xlsDataInfo.getRowIndex();
        int cellNum = xlsDataInfo.getCellNum();
        HSSFRow row = sheet.getRow(rowIndex);
        if(row==null){
            row=sheet.createRow(rowIndex);
        }
        HSSFCell cell = row.getCell(cellNum);
        if(cell==null){
            cell = row.createCell(cellNum);
        }
        if(StringUtils.isNotBlank(fixedReplaceName)){
            cell.setCellValue(fixedReplaceName);
        }
        else{
            String fixedName = xlsDataInfo.getData();
            if(fixedName.contains(MARK)){
                fixedName="";
            }
            cell.setCellValue(fixedName);
        }

    }
}

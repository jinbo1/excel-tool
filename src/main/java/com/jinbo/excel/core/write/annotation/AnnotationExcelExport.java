package com.jinbo.excel.core.write.annotation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.jinbo.excel.core.annotation.AnalyzeExcelAnnotationData;
import com.jinbo.excel.core.write.CommonExportXls;
import com.jinbo.excel.model.ColumnStuffExcelData;
import com.jinbo.excel.model.ExcelData;
import com.jinbo.excel.model.ExcelPositionInformation;
import com.jinbo.excel.model.SheetInformation;
import com.jinbo.excel.model.XlsDataInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * <p> Description: 导出表格类</p >
 *
 * <p> CreationTime: 2020/10/31 18:33
 * <br>Copyright: 2020 < a href=" ">Thunisoft</ a>
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */
@Slf4j
public class AnnotationExcelExport<T> {
    /**
     * 后缀
     */
    private static final String XLS = ".xls";

    /**
     * 注解表格导出
     * @param response response
     * @param excelData 数据
     * @return 导出状态
     */
    public  String  export(HttpServletResponse response, ExcelData<T> excelData, String name)  {
        List<ExcelData<T>> dataList =new ArrayList<>();
        dataList.add(excelData);
        return export(response,dataList,name);
    }

    /**
     * 导出多个sheet
     * @param response response
     * @param excelDataList 表格数据
     * @param name 文件名
     * @return 导出结果
     */
    public String export(HttpServletResponse response, List<ExcelData<T>> excelDataList,
                              String name){
        HSSFWorkbook wb = getWorkbook(excelDataList, name);
        CommonExportXls.fileTransmission(response, wb, name+ XLS);
        return "导出成功";
    }

    /**
     * 导出表格为HSSFWorkbook 形式
     * @param excelDataList 表格数据
     * @param name 名称
     * @return 数据结果
     */
    public HSSFWorkbook getWorkbook(List<ExcelData<T>> excelDataList, String name) {
        List<String> sheetNameList = new ArrayList<>();
        HSSFWorkbook wb = new HSSFWorkbook();
        for (ExcelData<T> excelData : excelDataList) {
            ColumnStuffExcelData columnStuffExcelData = AnalyzeExcelAnnotationData.dealWithExcelAnnotationData(excelData);
            String sheetName = dealSheetName(name, sheetNameList, columnStuffExcelData);
            HSSFSheet sheet = wb.createSheet(sheetName);
            List<ExcelPositionInformation> excelPositionInformationList =
                    columnStuffExcelData.getExcelPositionInformation();
            //构建sheet信息
            SheetInformation sheetInformation = buildSheetInformation(excelPositionInformationList);
            //设置样式等
            CommonExportXls.setFormContentStyle(wb, sheet, excelData.getData().size() + 1,
                    excelPositionInformationList.get(excelPositionInformationList.size() - 1).getColumn(), sheetInformation);
            //写入固定信息
            writeFixedInfo(sheet, excelPositionInformationList);
            //写入数据信息
            writeDataInfo(columnStuffExcelData, sheet, excelPositionInformationList);
        }
        return wb;
    }

    /**
     * 导出表格文件到指定路径
     * @param excelDataList 表格数据
     * @param path 路径
     * @param name 文件名称
     */
    public void saveFile(List<ExcelData<T>> excelDataList,String path, String name){
        if(!name.endsWith(XLS)){
            name+= XLS;
        }
        HSSFWorkbook sheets = getWorkbook(excelDataList, name);
        mkdir(path);
        try(OutputStream out = new FileOutputStream(path+name)) {
            sheets.write(out);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 新建文件夹
     * @param path 路径
     * @return 新建结果
     */
    public static boolean mkdir(String path){
        File file = new File(path);
        if(!file.exists()){
            return file.mkdirs();
        }
        return false;
    }

    /**
     * 构建sheet信息对象
     * @param excelPositionInformationList 表格位子表头等信息
     * @return sheet信息结果
     */
    private SheetInformation buildSheetInformation(List<ExcelPositionInformation> excelPositionInformationList) {
        List<XlsDataInfo> xlsDataInfoList = new ArrayList<>();
        Map<Integer, Short> rowHeightInformation = new HashMap<>();
        Map<Integer, Integer> columnWidthInformation = new HashMap<>();
        for (ExcelPositionInformation excelPositionInformation : excelPositionInformationList) {
            XlsDataInfo xlsDataInfo = XlsDataInfo.builder()
                    .data(excelPositionInformation.getAnnotationName())
                    .rowIndex(excelPositionInformation.getRow())
                    .cellNum(excelPositionInformation.getColumn()).build();
            xlsDataInfoList.add(xlsDataInfo);
            rowHeightInformation.put(excelPositionInformation.getRow(), excelPositionInformation.getHigh());
            columnWidthInformation.put(excelPositionInformation.getColumn(), excelPositionInformation.getWidth());
        }

        return SheetInformation.builder()
                .xlsDataInfos(xlsDataInfoList).rowHeightInformation(rowHeightInformation)
                .columnWidthInformation(columnWidthInformation).build();
    }

    /**
     * 写入固定信息
     * @param sheet sheet
     * @param excelPositionInformationList 表格表头等位子固定信息
     */
    private void writeFixedInfo(HSSFSheet sheet, List<ExcelPositionInformation> excelPositionInformationList) {
        for (ExcelPositionInformation excelPositionInformation : excelPositionInformationList) {
            sheet.getRow(excelPositionInformation.getRow()).getCell(
                    excelPositionInformation.getColumn()).setCellValue(excelPositionInformation.getAnnotationName());
        }
    }

    /**
     * 写入数据信息
     * @param columnStuffExcelData 列数据
     * @param sheet sheet sheet
     * @param excelPositionInformationList 表格表头等位子固定信息
     */
    private void writeDataInfo(ColumnStuffExcelData columnStuffExcelData, HSSFSheet sheet, List<ExcelPositionInformation> excelPositionInformationList) {

        List<Map<String, String>> dataList = columnStuffExcelData.getData();
        for (int i = 0; i < dataList.size(); i++) {
            Map<String, String> map = dataList.get(i);
            for (ExcelPositionInformation excelPositionInformation : excelPositionInformationList) {
                String dataStr = map.get(excelPositionInformation.getFieldName());
                int column = excelPositionInformation.getColumn();
                int row = excelPositionInformation.getRow() + i + 1;
                sheet.getRow(row).getCell(column).setCellValue(dataStr);
            }
        }
    }

    /**
     * 处理表格sheet名称
     * @param name 表格名称
     * @param sheetNameList sheet全部名称列表
     * @param columnStuffExcelData 注解信息
     * @return sheet名称
     */
    private String dealSheetName(String name, List<String> sheetNameList, ColumnStuffExcelData columnStuffExcelData) {
        String sheetName = columnStuffExcelData.getSheetName();
        sheetNameList.add(sheetName);
        int sheetNum = Collections.frequency(sheetNameList,sheetName);
        if(StringUtils.isBlank(sheetName)){
            sheetName = name;
        }
        if(sheetNum>1){
            sheetName = sheetName+"("+(sheetNum-1)+")";
        }
        return sheetName;
    }


}

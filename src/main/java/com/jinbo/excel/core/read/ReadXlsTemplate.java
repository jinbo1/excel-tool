package com.jinbo.excel.core.read;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;

import com.jinbo.excel.conf.ExcelToolProperties;
import com.jinbo.excel.model.SheetInformation;
import com.jinbo.excel.model.XlsDataInfo;
import com.jinbo.excel.model.CustomizeCellRangeAddress;

import lombok.extern.slf4j.Slf4j;

/**
 * <p> Description: 模板类，读取模板结构</p >
 * <p> CreationTime: 2020/10/31 16:41
 * <br>Copyright: 2020 < a href=" ">Thunisoft</ a>
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */
@Slf4j
public class ReadXlsTemplate {

    /**
     * 表格信息
     */
    private Map<String, List<SheetInformation>> xlsInformation;

    /**
     * 手动重载模板方法
     *
     * @return 重载是否成功消息
     */
    public String reloadXlsInformation() {
        try {
            xlsInformation.clear();
            this.readXlsFile();
        } catch (Exception e) {
            log.error("重载模板失败", e);
            return "重载模板失败";
        }
        return "重载模板成功";

    }

    /**
     * 获取表格行高信息
     *
     * @param sheet 表格
     * @return 好高信息
     */
    private Map<Integer, Short> getRowHeightInformation(Sheet sheet) {
        int rowNum = sheet.getPhysicalNumberOfRows();
        Map<Integer, Short> rowHeightInformationMap = new HashMap<>();
        for (int k = 0; k < rowNum; k++) {
            Row row = sheet.getRow(k);
            if (row == null) {
                continue;
            }
            Short height = row.getHeight();
            rowHeightInformationMap.put(k, height);
        }
        return rowHeightInformationMap;
    }

    /**
     * 读模板表格信息程序启动自动加载
     */
    @PostConstruct
    public void readXlsFile() {
        String directory = excelToolProperties.getDirectory();
        if (StringUtils.isBlank(directory)) {
            return;
        }
        List<File> files = loadXlsFile(directory);
        Map<String, List<SheetInformation>> xlsInformation = new HashMap<>();
        for (File file : files) {
            try {
                InputStream inputStream = new FileInputStream(file);
                String fileName = file.getName();
                // 1、通过工作簿工厂类来创建工作簿对象
                Workbook workbook = null;
                try {
                    workbook = WorkbookFactory.create(inputStream);
                } catch (IOException e) {
                    log.error("Io异常出错报错为", e);
                }
                //2、遍历工作簿下面的所有工作表
                if (workbook == null) {
                    log.error("workbook为空");
                    return;
                }
                List<SheetInformation> sheetInformationList = getSheetInfo(workbook);
                xlsInformation.put(fileName, sheetInformationList);
            } catch (FileNotFoundException e) {
                log.error("转换流失败", e);
                e.printStackTrace();
            }
        }
        this.xlsInformation = xlsInformation;
    }

    /**
     * 获取表格信息
     *
     * @param workbook workbook
     * @return 每个sheet表格信息
     */
    private List<SheetInformation> getSheetInfo(Workbook workbook) {
        int sheetNum = workbook.getNumberOfSheets();
        List<SheetInformation> sheetInformations = new ArrayList<>();
        for (int i = 0; i < sheetNum; i++) {
            //获取到单个工作表
            Sheet sheet = workbook.getSheetAt(i);
            String sheetName = sheet.getSheetName();
            List<XlsDataInfo> xlsDataInfos =
                    ReadXlsUtil.readXlsData(sheet);
            List<CustomizeCellRangeAddress> customizeCellRangeAddresses = readCellMergeRangeAddress(sheet);
            Map<Integer, Short> rowHeightInformation = getRowHeightInformation(sheet);
            SheetInformation sheetInformation = SheetInformation.builder().sheetName(sheetName)
                    .xlsDataInfos(xlsDataInfos)
                    .customizeCellRangeAddresses(customizeCellRangeAddresses).rowHeightInformation(rowHeightInformation)
                    .columnWidthInformation(getColumnWidth(sheet)).cellBackgroundColor(cellBackgroundColor(sheet))
                    .build();
            sheetInformations.add(sheetInformation);
        }
        return sheetInformations;
    }

    private Map<Integer, Map<Integer, Short>> cellBackgroundColor(Sheet sheet) {
        Map<Integer, Map<Integer, Short>> cellBackgroundColor = new HashMap<>();
        int rows = sheet.getLastRowNum();
        for (int r = 0; r < rows; r++) {
            HashMap<Integer, Short> row = new HashMap<>();
            int cells = sheet.getRow(r).getLastCellNum();
            for (int c = 0; c < cells; c++) {
                row.put(c, sheet.getRow(r).getCell(c).getCellStyle().getFillForegroundColor());
                cellBackgroundColor.put(r, row);
            }
        }
        return cellBackgroundColor;
    }

    /**
     * 获取列宽
     *
     * @param sheet sheet
     * @return 列宽对象
     */
    private HashMap<Integer, Integer> getColumnWidth(Sheet sheet) {
        int cellNum = sheet.getRow(0).getPhysicalNumberOfCells();
        HashMap<Integer, Integer> columnWidthInformation = new HashMap<>();
        for (int k = 0; k < cellNum; k++) {
            columnWidthInformation.put(k, sheet.getColumnWidth(k));
        }
        return columnWidthInformation;
    }

    /**
     * 保存合并的地址
     *
     * @param sheet sheet
     */
    private List<CustomizeCellRangeAddress> readCellMergeRangeAddress(Sheet sheet) {
        List<CustomizeCellRangeAddress> customizeCellRangeAddresses = new ArrayList<>();
        int sheetMergerCount = sheet.getNumMergedRegions();
        for (int j = 0; j < sheetMergerCount; j++) {
            CellRangeAddress cellAddresses = sheet.getMergedRegion(j);
            CustomizeCellRangeAddress customizeCellRangeAddress =
                    CustomizeCellRangeAddress.builder().firstColumn(cellAddresses.getFirstColumn())
                            .firstRow(cellAddresses.getFirstRow()).lastColumn(cellAddresses.getLastColumn())
                            .lastRow(cellAddresses.getLastRow()).build();
            customizeCellRangeAddresses.add(customizeCellRangeAddress);

        }
        return customizeCellRangeAddresses;
    }



    @Autowired
    private ExcelToolProperties excelToolProperties;

    /**
     * 从配置路径中加载文件
     *
     * @return 文件列表
     */
    private List<File> loadXlsFile(String directory) {
        File file = FileUtils.getFile(directory);
        if (file == null || !file.isDirectory()) {
            log.error("无法加载模板文件路径{}", directory);
            throw new NullPointerException();
        }
        Collection<File> relationConfigFiles = FileUtils.listFiles(file,
                FileFilterUtils.suffixFileFilter(".xls"),
                DirectoryFileFilter.DIRECTORY);
        if (CollectionUtils.isEmpty(relationConfigFiles)) {
            log.error("加载模板文件路径{},没有xls模板文件", directory);
        }
        return new ArrayList<>(relationConfigFiles);
    }

    /**
     * 获取表格信息
     *
     * @return 返回表格信息
     */
    public Map<String, List<SheetInformation>> getXlsInformation() {
        return this.xlsInformation;
    }
}

package com.jinbo.excel.core.read;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.jinbo.excel.model.XlsDataInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * <p> Description: </p >
 *
 * <p> CreationTime: 2021/8/20 9:54
 * <br>Copyright: 2021 < a href=" ">Thunisoft</ a>
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */
@Slf4j
public class ReadXlsData {

    public List<List<XlsDataInfo>> getXlsData(InputStream inputStream) {
        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(inputStream);
        } catch (IOException e) {
            log.error("Io异常出错报错为", e);
        }
        if (workbook == null) {
            log.error("workbook为空");
            return Collections.emptyList();
        }
        int sheetNum = workbook.getNumberOfSheets();
        List<List<XlsDataInfo>> data = new ArrayList<>();
        for (int i = 0; i < sheetNum; i++) {
            //获取到单个工作表
            Sheet sheet = workbook.getSheetAt(i);
            data.add(ReadXlsUtil.readXlsData(sheet));
        }
        return data;

    }
}

package com.jinbo.excel.core.read;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.jinbo.excel.model.XlsDataInfo;

/**
 * <p> Description: </p >
 *
 * <p> CreationTime: 2021/8/20 13:29
 * <br>Copyright: 2021 < a href=" ">Thunisoft</ a>
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */
public final class ReadXlsUtil {
    /**
     * 保存固定名字的坐标
     *
     * @param sheet sheet
     */
    public static List<XlsDataInfo> readXlsData(Sheet sheet) {
        int rowNum = sheet.getPhysicalNumberOfRows();
        List<XlsDataInfo> xlsDataInfos = new ArrayList<>();
        for (int k = 0; k < rowNum; k++) {
            Row row = sheet.getRow(k);
            if (row == null) {
                continue;
            }
            int cellNum = row.getLastCellNum();
            for (int g = 0; g < cellNum; g++) {
                if (row.getCell(g) == null) {
                    continue;
                }
                String fixedName = getCellValue(row.getCell(g));
                if (StringUtils.isNotBlank(fixedName)) {
                    XlsDataInfo xlsDataInfo =
                            XlsDataInfo.builder().rowIndex(k).cellNum(g).data(fixedName).build();
                    xlsDataInfos.add(xlsDataInfo);
                }

            }
        }
        return xlsDataInfos;
    }


    /**
     * 获取表格里面的值
     *
     * @param cell 格子
     * @return 值
     */
    private static String getCellValue(Cell cell) {
        String cellValue;
        // 以下是判断数据的类型
        switch (cell.getCellType()) {
            case NUMERIC:
                if (org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    cellValue = sdf
                            .format(org.apache.poi.ss.usermodel.DateUtil.getJavaDate(cell.getNumericCellValue()));
                } else {
                    DataFormatter dataFormatter = new DataFormatter();
                    cellValue = dataFormatter.formatCellValue(cell);
                }
                break;
            case STRING:
                cellValue = cell.getStringCellValue();
                break;
            case BOOLEAN:
                cellValue = cell.getBooleanCellValue() + "";
                break;
            case FORMULA:
                cellValue = cell.getCellFormula() + "";
                break;
            case BLANK:
                cellValue = "";
                break;
            case ERROR:
                cellValue = "非法字符";
                break;
            default:
                cellValue = "未知类型";
                break;
        }
        return cellValue;
    }
}

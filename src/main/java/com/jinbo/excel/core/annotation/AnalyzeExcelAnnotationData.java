package com.jinbo.excel.core.annotation;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.jinbo.excel.model.ColumnInfo;
import com.jinbo.excel.model.ColumnStuffExcelData;
import com.jinbo.excel.model.Excel;
import com.jinbo.excel.model.ExcelData;
import com.jinbo.excel.model.ExcelPositionInformation;

import lombok.extern.slf4j.Slf4j;

/**
 * <p> Description: 解析带注解表格数据</p >
 *
 * <p> CreationTime: 2020/12/21 10:27
 * <br>Copyright: 2020 < a href=" ">Thunisoft</ a>
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */
@Slf4j
public class AnalyzeExcelAnnotationData {
    /**
     * 日期类型字段
     */
    private static final String DATE = "java.util.Date";

    /**
     * 处理以对象方式，带注解的数据
     * @param excelData 数据
     * @return 处理后的时候
     */
    public  static <T> ColumnStuffExcelData dealWithExcelAnnotationData(ExcelData<T> excelData){
        List<T> data = excelData.getData();
        Class<T> classType = excelData.getClassType();
        ColumnInfo columnInfo = excelData.getColumnInfo();
        List<Field> declaredFields = Arrays.stream(classType.getDeclaredFields()).collect(Collectors.toList());
        if(columnInfo !=null && !columnInfo.getColumnName().isEmpty()) {
            List<String> columnNameList = columnInfo.getColumnName();
            Boolean isExclude = columnInfo.getIsExclude();
            if(isExclude==null?true:isExclude){
                declaredFields = declaredFields.stream().filter(field ->
                        !columnNameList.contains(field.getName())).collect(Collectors.toList());
            }else{
                declaredFields = declaredFields.stream().filter(field ->
                        columnNameList.contains(field.getName())).collect(Collectors.toList());
            }


        }
        List<Map<String, String>> excelListDate = analyzeExcelData(data, declaredFields);
        List<ExcelPositionInformation> excelPositionInformationList = analyzeFieldInformation(declaredFields);
        ColumnStuffExcelData columnStuffExcelData = new ColumnStuffExcelData();
        columnStuffExcelData.setData(excelListDate);
        columnStuffExcelData.setExcelPositionInformation(
                excelPositionInformationList.stream().sorted(Comparator
                        .comparing(ExcelPositionInformation::getColumn)).collect(Collectors.toList()));

        Excel classAnnotation = classType.getAnnotation(Excel.class);
        if(classAnnotation!=null){
            columnStuffExcelData.setSheetName(classAnnotation.name());
        }
        return columnStuffExcelData;
    }

    /**
     * 解析字段信息
     * @param declaredFields 字段信息
     * @return 解析完的字段信息（注解信息，字段名称信息等）
     */
    private static List<ExcelPositionInformation> analyzeFieldInformation(List<Field> declaredFields) {
        List<ExcelPositionInformation> excelPositionInformationList = new ArrayList<>();
        for(Field field : declaredFields){
            if(!field.isAccessible()){
                field.setAccessible(true);
            }
            ExcelPositionInformation excelPositionInformation = new ExcelPositionInformation();
            Excel excel = field.getAnnotation(Excel.class);
            if(excel!=null){
                excelPositionInformation.setAnnotationName(excel.name());
                excelPositionInformation.setColumn(excel.column());
                excelPositionInformation.setRow(excel.row());
                excelPositionInformation.setFieldName(field.getName());
                excelPositionInformation.setHigh(excel.high());
                excelPositionInformation.setWidth(excel.width());
                excelPositionInformationList.add(excelPositionInformation);
            }
        }
        List<ExcelPositionInformation> setColumnInfo =
                excelPositionInformationList.stream().filter(f -> f.getColumn() != -1)
                .sorted(Comparator.comparing(ExcelPositionInformation::getColumn)).collect(Collectors.toList());
        setColumnInfo.addAll(
                excelPositionInformationList.stream().filter(f -> f.getColumn() == -1).collect(Collectors.toList()));
        for(int i=0;i<setColumnInfo.size();i++){
            setColumnInfo.get(i).setColumn(i);
        }
        return setColumnInfo;
    }

    /**
     * 解析表格数据将泛型数据解析成Map
     * @param data 数据
     * @param declaredFields 字段信息
     * @param <T> 泛型
     * @return 处理完的Map数据
     */
    private static <T> List<Map<String, String>> analyzeExcelData(List<T> data, List<Field> declaredFields) {
        return data.stream().map(d -> declaredFields.stream().collect(Collectors.toMap(Field::getName, field -> {
            String resultObj = null;
            field.setAccessible(true);
            Excel excel = field.getAnnotation(Excel.class);
            try {
                String dataType = field.getType().getName();
                if(excel!=null){
                    if(DATE.equals(dataType)&&field.get(d) !=null){
                        SimpleDateFormat dateFormat = new SimpleDateFormat(excel.dateFormat());
                        resultObj = dateFormat.format(field.get(d));
                    }else{
                        if(field.get(d)==null){
                            resultObj = excel.defaultValue();
                        }else{
                            resultObj = field.get(d)+"";
                        }
                        if(StringUtils.isBlank(resultObj)){
                            resultObj = excel.defaultValue();
                        }
                    }
                }
            } catch (IllegalArgumentException | IllegalAccessException e) {
                log.error("获取字段异常",e);
                e.printStackTrace();
            }
            return Optional.ofNullable(resultObj).orElse("");
        }, (k1, k2) -> k2))).collect(Collectors.toList());
    }
}
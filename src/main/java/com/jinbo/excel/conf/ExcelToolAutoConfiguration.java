package com.jinbo.excel.conf;

import com.jinbo.excel.core.read.ReadXlsData;
import com.jinbo.excel.core.read.ReadXlsTemplate;
import com.jinbo.excel.core.write.annotation.AnnotationExcelExport;
import com.jinbo.excel.core.write.template.ExportXls;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p> Description: </p >
 *
 * <p> CreationTime: 2020/11/11 20:34
 * <br>Copyright: 2020 < a href=" ">Thunisoft</ a>
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */
@Configuration
@EnableConfigurationProperties(ExcelToolProperties.class)
public class ExcelToolAutoConfiguration {
    @Bean
    public ReadXlsTemplate readXlsTemplate(){
        return new ReadXlsTemplate();
    }
    @Bean
    public ExportXls exportTemplateXls(){
        return new ExportXls(this.readXlsTemplate());
    }
    @Bean
    public AnnotationExcelExport exportXls(){
        return new AnnotationExcelExport();
    }
    @Bean
    public ReadXlsData readXlsData(){
        return new ReadXlsData();
    }
}

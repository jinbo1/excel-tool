package com.jinbo.excel.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p> Description: </p >
 *
 * <p> CreationTime: 2020/11/11 20:42
 * <br>Copyright: 2020 < a href=" ">Thunisoft</ a>
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */

@Data
@ConfigurationProperties(prefix = "excel.tool",ignoreInvalidFields = true)
public class ExcelToolProperties {
    /**
     * 表格模板地址
     */
    private String directory;
}

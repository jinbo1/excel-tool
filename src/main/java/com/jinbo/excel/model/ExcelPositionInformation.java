package com.jinbo.excel.model;

import lombok.Data;

/**
 * <p> Description: 表格位置信息</p >
 *
 * <p> CreationTime: 2020/12/18 15:30
 * <br>Copyright: 2020 < a href=" ">Thunisoft</ a>
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */
@Data
public class ExcelPositionInformation {
    /**
     * 字段名称
     */
    private String fieldName;
    /**
     * 列号
     */
    private Integer column;
    /**
     * 行号
     */
    private int row;
    /**
     * 注解信息
     */
    private String annotationName;
    /**
     * 高
     */
    private short high;
    /**
     * 宽
     */
    private int width;
}

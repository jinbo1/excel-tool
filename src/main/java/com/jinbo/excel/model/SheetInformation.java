package com.jinbo.excel.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * <p> Description: </p >
 *
 * <p> CreationTime: 2020/10/31 14:28
 * <br>Copyright: 2020 < a href=" ">Thunisoft</ a>
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */
@Data
@Builder
public class SheetInformation {
    /**
     * sheet名称
     */
    String sheetName;
    /**
     * 固定名字信息
     */
    List<XlsDataInfo> xlsDataInfos;
    /**
     * 合并单元格信息
     */
    List<CustomizeCellRangeAddress> customizeCellRangeAddresses;

    /**
     * 行高信息
     */
    Map<Integer,Short> rowHeightInformation;
    /**
     * 列宽信息
     */
    Map<Integer,Integer> columnWidthInformation;

    /**
     * 单元格背景色
     */
    Map<Integer, Map<Integer, Short>> cellBackgroundColor;
}

package com.jinbo.excel.model;

import lombok.Builder;
import lombok.Data;

/**
 * <p> Description: </p >
 *
 * <p> CreationTime: 2020/9/29 14:59
 * <br>Copyright: 2020 < a href=" ">Thunisoft</ a>
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */
@Builder
@Data
public class XlsDataInfo {
    /**
     * 行位子
     */
    private int rowIndex;
    /**
     * 列号
     */
    private int cellNum;
    /**
     * 固定名称
     */
    private String data;

}

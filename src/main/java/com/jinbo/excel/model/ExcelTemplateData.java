package com.jinbo.excel.model;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * <p> Description: 表格模板数据</p >
 *
 * <p> CreationTime: 2020/11/2 9:13
 * <br>Copyright: 2020 < a href=" ">Thunisoft</ a>
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */
@Data
public class ExcelTemplateData {
    /**
     * 标准数据
     */
    List<List<String>> standardData;
    /**
     * 特殊点数据
     */
    Map<String,String> specialData;

}

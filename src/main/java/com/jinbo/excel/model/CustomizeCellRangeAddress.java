package com.jinbo.excel.model;

import lombok.Builder;
import lombok.Data;

/**
 * <p> Description: </p >
 *
 * <p> CreationTime: 2020/9/29 14:59
 * <br>Copyright: 2020 < a href=" ">Thunisoft</ a>
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */
@Builder
@Data
public class CustomizeCellRangeAddress {
    /**
     * 起始行
     */
    private int firstRow;
    /**
     * 结束行
     */
    private int lastRow;
    /**
     * 起始列
     */
    private int firstColumn;
    /**
     * 结束列
     */
    private int lastColumn;
    /**
     * 表格ID
     */
    private String sheetId;
}

package com.jinbo.excel.model;

import java.lang.annotation.*;

/**
 * <p> Description: 表格注解</p >
 *
 * <p> CreationTime: 2020/11/2 9:13
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.TYPE})
@Documented
public @interface Excel {
    /**
     * 行号
     */
    int row() default 0;
    /**
     * 列序号
     */
    int column() default -1;
    /**
     * 表格列名
     */
    String name() default "";
    /**
     * 数据为空默认值
     */
    String defaultValue() default "";
    /**
     * 日期类型格式化，默认到天
     */
    String dateFormat() default "YYYY-MM-dd";
    /**
     * 宽度，默认35
     */
    int width() default 30*256;
    /**
     *高度，默认是0，自适应高度
     */
    short high() default 0;
}

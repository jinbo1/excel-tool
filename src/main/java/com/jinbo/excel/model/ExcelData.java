package com.jinbo.excel.model;

import lombok.Data;


import java.util.List;

/**
 * <p> Description: 表格数据</p >
 *
 * <p> CreationTime: 2021/1/19 17:29
 * <br>Copyright: 2021 < a href=" ">Thunisoft</ a>
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */
@Data
public class ExcelData<T> {
    /**
     * 数据
     */
    private List<T> data;
    /**
     * 类类型
     */
    private Class<T> classType;
    /**
     * 字段信息
     */
    private ColumnInfo columnInfo;
}

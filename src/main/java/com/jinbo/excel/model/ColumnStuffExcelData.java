package com.jinbo.excel.model;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * <p> Description: 列填录数据方式，表格信息</p >
 *
 * <p> CreationTime: 2020/12/18 15:40
 * <br>Copyright: 2020 < a href=" ">Thunisoft</ a>
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */
@Data
public class ColumnStuffExcelData {
    /**
     * 数据信息
     */
    List<Map<String,String>> data;
    /**
     * 位子信息
     */
    List<ExcelPositionInformation> excelPositionInformation;
    /**
     * 特殊点数据
     */
    Map<String,String> specialData;
    /**
     * sheet名称
     */
    String sheetName;
}

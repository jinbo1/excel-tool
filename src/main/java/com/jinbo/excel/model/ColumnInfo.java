package com.jinbo.excel.model;

import lombok.Data;

import java.util.List;

/**
 * <p> Description: 列信息</p >
 *
 * <p> CreationTime: 2021/2/22 9:16
 * <br>Copyright: 2021 < a href=" ">Thunisoft</ a>
 * <br>Email: < a href="mailto:jinbo-1@thunisoft.com">jinbo-1@thunisoft.com</ a></p >
 *
 * @author jinbo
 * @version 1.0
 */
@Data
public class ColumnInfo {
    /**
     * 列名
     */
    List<String> columnName;
    /**
     * 是排除还是包含
     */
    Boolean isExclude;
}
